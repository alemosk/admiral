# AM - Python Test Assignment solution

## Requirements
* Python 3.6+ with venv module
* Django 3.0
* Git

## Installation on linux (localhost)
~~~bash
git clone git@bitbucket.org:alemosk/admiral.git
cd admiral

# VirtualEnv setup
python3 -m venv venv
source venv/bin/activate

# Update pip to latest version
pip install -U pip setuptools

# Install project dependencies
pip install -r requirements.txt

# Apply migrations
./manage.py migrate

# Create root category
./manage.py loaddata fixtures/root_category.json

# Run unit tests
./manage.py test

# And finally run server
./manage.py runserver

# Test requests (curl or other client)
# Post data
curl -H "Content-Type: application/json" --data @fixtures/test.json http://localhost:8000/categories/

# Get data
curl http://localhost:8000/categories/8/

~~~
If for command runsever not set extra parameters server will work at link [http://localhost:8000](http://localhost:8000)

# Urls
`POST /categories/` - used to create new categories with root as parent

`GET /categories/<id>/` - used to retrieve category by category `id` 
