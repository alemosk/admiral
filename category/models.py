from django.db import models


class Node(models.Model):
    """
    Category node.
    Using Nested Set with parent data model.
    Parent used for fastest siblings retrieve.
    """
    name = models.TextField(max_length=255, unique=True)
    left = models.IntegerField()
    right = models.IntegerField()
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True)

    def serialize(self):
        """
        Serialize node object to dictionary
        :return: dict
        """
        parents = Node.objects.filter(
            left__lt=self.left,
            right__gt=self.right
        ).exclude(name='root').order_by('-left').values('id', 'name')

        siblings = Node.objects.filter(
            parent=self.parent
        ).exclude(id=self.id).values('id', 'name')

        children = Node.objects.filter(parent=self).values('id', 'name')

        return {
            'id': self.id,
            'name': self.name,
            'parents': list(parents),
            'siblings': list(siblings),
            'children': list(children)
        }

    @staticmethod
    def has_only_unique_names(serialized_node, names=None, generation=0):
        """
        Check request data for unique names
        :param serialized_node:
        :param names:
        :param generation:
        :return:
        """
        if names is None:
            names = []

        if type(serialized_node) != dict:
            return (False, names) if generation > 0 else False

        name = serialized_node.get('name')
        if name is None or len(name) == 0:
            return (False, names) if generation > 0 else False

        names.append(name)
        for children_node in serialized_node.get('children', []):
            unique, names = Node.has_only_unique_names(
                children_node, names, generation+1
            )

            if unique is False:
                return (False, names) if generation > 0 else False

        is_unique = len(names) == len(set(names))
        return (is_unique, names) if generation > 0 else is_unique

    @staticmethod
    def serialized_is_valid(serialized_node):
        """
        Проверить полученные категории на уникальность до сохранения бд
        Validate request data of category
        Name must be not empty string. Children must be array.
        :param serialized_node:
        :return:
        """
        allowed_fields = ['name', 'children']
        if not type(serialized_node) == dict or not \
                all([x in allowed_fields for x in serialized_node.keys()]):
            return False

        if not type(serialized_node.get('name')) == str or \
                len(serialized_node.get('name').strip()) == 0 or \
                len(serialized_node.get('name').strip()) > 255 or \
                type(serialized_node.get('children')) not in [type(None), list]:
            return False

        result = True
        for children in serialized_node.get('children', {}):
            result = Node.serialized_is_valid(children)
            if result is False:
                break

        return result

    def create_children(self, serialized_node):
        """
        Converts request data to Node object.
        """
        # data should be valid
        if Node.serialized_is_valid(serialized_node):
            node = Node(
                parent=self,
                name=serialized_node.get('name').strip(),
                left=self.right,
                right=self.right + 1
            )
            node.save()

            for child in serialized_node.get('children', []):
                node.create_children(child)

            self.right = node.right + 1
            self.save()
        else:
            node = None

        return node

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()
