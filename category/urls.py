from django.urls import path
from . import views


urlpatterns = [
    path('', views.create_category, name='create-category'),
    path('<int:category_id>/', views.show_category, name='show-category'),
]
