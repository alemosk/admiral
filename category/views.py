import json
from json import JSONDecodeError

from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.http import HttpResponse

from category.models import Node


def json_response(data, **kwargs):
    """
    Json response helper
    :param data:
    :param kwargs:
    :return: HttpResponse
    """
    return HttpResponse(
        json.dumps(data, indent=4),
        content_type='application/json',
        **kwargs
    )


def create_category(request):
    """
    Create category endpoint
    :param request:
    :return: HttpResponse
    """
    response = json_response({
        'success': False,
        'message': 'Method not allowed'
    }, status=405)

    if request.method == 'POST':
        data = None
        try:
            data = json.loads(request.body)
            result = Node.serialized_is_valid(data)
        except JSONDecodeError:
            result = False

        if result is True:
            if Node.has_only_unique_names(data):
                try:
                    root = Node.objects.get(name='root')
                    root.create_children(data)
                    response = json_response({'success': True})
                except IntegrityError as e:
                    response = json_response({
                        'success': False,
                        'message': 'Category with specified name already exist'
                    }, status=400)
            else:
                response = json_response({
                    'success': False,
                    'message': 'Data contains non unique category names'
                }, status=400)
        else:
            response = json_response({
                'success': False,
                'message': 'Invalid json data schema'
            }, status=400)

    return response


def show_category(request, category_id):
    """
    Show category endpoint
    :param request:
    :param category_id:
    :return: HttpResponse
    """
    try:
        response = json_response({
            'success': False,
            'message': 'Method not allowed'
        }, status=405)

        if request.method == 'GET':
            response = json_response(
                Node.objects.get(id=category_id).serialize()
            )
    except ObjectDoesNotExist:
        response = json_response({
            'success': False,
            'message': 'Category not found'
        }, status=404)

    return response
