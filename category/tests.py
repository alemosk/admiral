import json

from django.test import TestCase
from django.urls import reverse

from .models import Node

TEST_DATA = {
    "name": "Category 1",
    "children": [
        {
            "name": "Category 1.1",
            "children": [
                {
                    "name": "Category 1.1.1",
                    "children": [
                        {
                            "name": "Category 1.1.1.1"
                        },
                        {
                            "name": "Category 1.1.1.2"
                        },
                        {
                            "name": "Category 1.1.1.3"
                        }
                    ]
                },
                {
                    "name": "Category 1.1.2",
                    "children": [
                        {
                            "name": "Category 1.1.2.1"
                        },
                        {
                            "name": "Category 1.1.2.2"
                        },
                        {
                            "name": "Category 1.1.2.3"
                        }
                    ]
                }
            ]
        },
        {
            "name": "Category 1.2",
            "children": [
                {
                    "name": "Category 1.2.1"
                },
                {
                    "name": "Category 1.2.2",
                    "children": [
                        {
                            "name": "Category 1.2.2.1"
                        },
                        {
                            "name": "Category 1.2.2.2"
                        }
                    ]
                }
            ]
        }
    ]
}


class CategoryTestCase(TestCase):
    def setUp(self):
        Node.objects.create(name='root', left=0, right=1)

    def test_node_create_children(self):
        root = Node.objects.get(name='root')
        model = root.create_children(TEST_DATA)

        self.assertEqual(model.right, 30)
        self.assertEqual(root.right, 31)

    def test_node_serialized_is_valid(self):
        self.assertTrue(Node.serialized_is_valid(TEST_DATA))
        self.assertTrue(Node.serialized_is_valid({
            'name': 'test',
            'children': []}
        ))
        self.assertTrue(Node.serialized_is_valid({'name': 'test'}))

        self.assertFalse(Node.serialized_is_valid({'name': ''}))
        self.assertFalse(Node.serialized_is_valid({
            'name': 'test',
            'children': ''
        }))
        self.assertFalse(Node.serialized_is_valid({}))

    def test_request_show_category(self):
        root = Node.objects.get(name='root')
        root.create_children(TEST_DATA)

        response = self.client.get(reverse('show-category', args=(9,)))

        self.assertContains(response, 'Category')

    def test_request_create_category(self):
        response = self.client.post(
            reverse('create-category'),
            json.dumps(TEST_DATA),
            content_type='application/json'
        )

        self.assertContains(response, 'true')

        count = Node.objects.count()

        self.assertEqual(count, 16)

    def test_unique_constraint(self):
        response = self.client.post(
            reverse('create-category'),
            json.dumps(TEST_DATA),
            content_type='application/json'
        )

        self.assertContains(response, 'true')

        response = self.client.post(
            reverse('create-category'),
            json.dumps(TEST_DATA),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, 400)

    def test_has_only_unique_names(self):
        self.assertFalse(
            Node.has_only_unique_names({
                'name': '12',
                'children': [{
                    'name': '12'
                }]
            })
        )

        self.assertFalse(
            Node.has_only_unique_names(None)
        )

        self.assertTrue(
            Node.has_only_unique_names({
                'name': '12',
                'children': [{'name': '13'}]
            })
        )
